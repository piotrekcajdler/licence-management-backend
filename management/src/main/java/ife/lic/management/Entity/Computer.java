package ife.lic.management.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
public class Computer {
    @Id
    @GeneratedValue(strategy =GenerationType.AUTO)
    private Long id;

    @Nullable
    private String name;

    @NotNull
    private String Location;

    @Column(name="usersOfComputer")
    @ElementCollection(targetClass=String.class)
    private List<String> usersOfComputer = new ArrayList<>();


    @ManyToMany(mappedBy = "computerList",cascade = CascadeType.PERSIST)
    private List<Person> workers = new ArrayList<>();
    @NotNull
    private boolean IsMultiuser;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private List<Metric> metricList = new ArrayList<>();

    @Nullable
    private String description = "";

    private boolean stillUsed = true;

    public Computer(){}


    public Computer(String name, String location, boolean isMultiuser, boolean stillUsed) {
        this.name = name;
        Location = location;
        IsMultiuser = isMultiuser;
        this.stillUsed = stillUsed;
    }
    public Computer(String name, String location, boolean isMultiuser, boolean stillUsed,String description) {
        this.name = name;
        Location = location;
        IsMultiuser = isMultiuser;
        this.stillUsed = stillUsed;
        this.description = description;
    }

    public Computer(String location, boolean isMultiuser) {
        name = "new Computer";
        Location = location;
        IsMultiuser = isMultiuser;
        stillUsed = true;
    }
}
