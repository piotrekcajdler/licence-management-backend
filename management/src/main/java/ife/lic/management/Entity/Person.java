package ife.lic.management.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Person {
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Role> roles = new HashSet<>();


    @NotNull
    @Size(min = 5)
    @Email
    private String email;

    @NotNull
    @Size(min = 8)
    private String password;

    @JsonIgnore
    @ManyToMany
    private Set<Computer> computerList;

    @NotNull
    private String name;

    @NotNull
    private String surname;

    @ManyToOne
    private Institute institute;


    public boolean active = true;

    public Person(@Size(min = 5) @Email String email, @Size(min = 8) String password, String name, String surname) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.surname = surname;
    }



    public Person(@Size(min = 5) @Email String email, @Size(min = 8) String password, String name, String surname, Institute institute) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.institute = institute;
    }
}
