package ife.lic.management.Entity;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Metric {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    @NotNull
    @ManyToOne
     private License license;

    @ManyToOne
    private Computer computer;
    @NotNull
    private String InstallDate;
    @Nullable
    private String UninstallDate;
    @NotNull
    private String WhoInstalled;
    @Nullable
    private String WhoUninstalled;
    @NotNull
    private boolean isDeleted = false;
    @NotNull
    private String whoAgreed;
    @Nullable
    private String description = " ";

    public Metric(){}


    public Metric(String installDate, String uninstallDate, String whoInstalled, String whoUninstalled, boolean isDeleted, String whoAgreed, String description) {
        InstallDate = installDate;
        UninstallDate = uninstallDate;
        WhoInstalled = whoInstalled;
        WhoUninstalled = whoUninstalled;
        this.isDeleted = isDeleted;
        this.whoAgreed = whoAgreed;
        this.description = description;
    }

    public Metric(String installDate, String uninstallDate, String whoInstalled, String whoUninstalled, String description) {
        InstallDate = installDate;
        UninstallDate = uninstallDate;
        WhoInstalled = whoInstalled;
        WhoUninstalled = whoUninstalled;
        isDeleted = false;
        this.description = description;
    }
    public Metric(String installDate, String uninstallDate, String whoInstalled, String whoUninstalled) {
        InstallDate = installDate;
        UninstallDate = uninstallDate;
        WhoInstalled = whoInstalled;
        WhoUninstalled = whoUninstalled;
        isDeleted = false;
        this.description = "";
    }



}
