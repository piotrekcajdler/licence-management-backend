package ife.lic.management.Entity;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "files")
@Getter
@Setter
public class File {
    @Id

    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    @Column(name = "name")
    private String name;

    @ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.MERGE,mappedBy = "files")
    @JsonIgnore
    private Set<License> licenses =new HashSet<>();

    @Column(name = "path")
    private String path;

    public File(){}


    public File(long id, String name, String path) {
        this.id = id;
        this.name = name;
        this.path = path;
    }

    public File(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public File(String name) {
        this.name = name;
    }



}
