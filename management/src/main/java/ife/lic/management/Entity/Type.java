package ife.lic.management.Entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class Type {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    @NotNull
    private String name;
    private String description;

    public Type(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @JsonIgnore
    @OneToMany( mappedBy = "type")
    private List<License> licenses;
    public  Type(String name){
        this.name = name;
    }
    public Type(){}
    public Type(long id){
        this.id = id;}


}
