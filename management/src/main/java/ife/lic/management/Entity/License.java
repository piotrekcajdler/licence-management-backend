package ife.lic.management.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
@Getter
@Setter
public class License {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @NotNull
    private String name;
    @ManyToOne
    @NotNull
    private Type type;
    @JsonIgnore
    @OneToMany(mappedBy = "license")
    private List<Metric> metricList;
    @NotNull
    private String PurchaseDate;
    @NotNull
    private String ExpiryDate;
    @NotNull
    private boolean isActive;
    @Nullable
    private Long AvailableInstallations;
    @Nullable
    private String key;
    @Nullable
    private String description;
    public License(){}

    @ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinTable(name = "lic_file",
            joinColumns = @JoinColumn(name = "lic_id"),
            inverseJoinColumns = @JoinColumn(name = "file_id"))
    Set<File> files = new HashSet<>();

    public License(String name, Type type, String purchaseDate, String expiryDate, boolean isActive, Long availableInstallations, String key) {
        this.name = name;
        this.type = type;
        PurchaseDate = purchaseDate;
        ExpiryDate = expiryDate;
        this.isActive = isActive;
        AvailableInstallations = availableInstallations;
        this.key = key;
    }
    public License(String name, Type type, String purchaseDate, String expiryDate, boolean isActive, Long availableInstallations, String key,String description) {
        this.name = name;
        this.type = type;
        PurchaseDate = purchaseDate;
        ExpiryDate = expiryDate;
        this.isActive = isActive;
        AvailableInstallations = availableInstallations;
        this.key = key;
        this.description = description;
    }


    public License(String name, Type type, String purchaseDate, String expiryDate, Long availableInstallations, String key) {
        this.name = name;
        this.type = type;
        PurchaseDate = purchaseDate;
        ExpiryDate = expiryDate;
        this.isActive = true;
        AvailableInstallations = availableInstallations;
        this.key = key;
    }

    public License(String name, String purchaseDate, String expiryDate, Long availableInstallations, String key) {
        this.name = name;
        PurchaseDate = purchaseDate;
        ExpiryDate = expiryDate;
        this.isActive = true;
        AvailableInstallations = availableInstallations;
        this.key = key;
    }
    public License(String name, String purchaseDate, String expiryDate, Long availableInstallations, String key,String description) {
        this.name = name;
        PurchaseDate = purchaseDate;
        ExpiryDate = expiryDate;
        this.isActive = true;
        AvailableInstallations = availableInstallations;
        this.key = key;
        this.description = description;
    }



}
