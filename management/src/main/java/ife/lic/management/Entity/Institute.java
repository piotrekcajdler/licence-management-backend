package ife.lic.management.Entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
public class Institute {
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    @NotNull
    private String name;

    @Nullable
    private Long parentID;

    @JsonIgnore
    @OneToMany(mappedBy = "institute",fetch = FetchType.EAGER)
    List<Person> people;

    public Institute(){}

    public Institute(String name, Long parentID) {
        this.name = name;
        this.parentID = parentID;
    }
    public Institute(long id) {
       this.id = id;
    }

}
