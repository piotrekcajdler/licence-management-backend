package ife.lic.management.Controller;

import ife.lic.management.View.LicenseData;
import ife.lic.management.Services.LicenseService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin("http://localhost:4200")
@RequestMapping("/license")
@RestController
public class LicenseController {
    private final LicenseService licenseService;


    public LicenseController(LicenseService licenseService) {
        this.licenseService = licenseService;
    }

    @GetMapping("/{lic}")
    public ResponseEntity<?> getByName(@PathVariable String lic){
        return  licenseService.findLicByName(lic);
    }

    @PostMapping("/create")
    public ResponseEntity<?> createInstitute(@RequestBody LicenseData lic){
        return  licenseService.createLicense(lic);
    }
    @GetMapping("/all")
    public ResponseEntity<?> getAllInstitutes(){
        return licenseService.getAllLicences();
    }

    @DeleteMapping("/{id}")
    private ResponseEntity<?> deleteLicence(@PathVariable long id) {
        return licenseService.deleteLicence(id);
    }

    @PatchMapping("/update/{id}")
    private ResponseEntity<?> updateLicence(@PathVariable long id, @RequestBody LicenseData licenseData){ return  licenseService.modifyLicence(id,licenseData);}

}
