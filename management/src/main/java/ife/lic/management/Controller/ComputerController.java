package ife.lic.management.Controller;

import ife.lic.management.Services.ComputerService;
import ife.lic.management.Services.InstituteService;
import ife.lic.management.View.CompData;
import ife.lic.management.View.LicenseData;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin("http://localhost:4200")
@RequestMapping("/computer")
@RestController
public class ComputerController {
    private final ComputerService computerService;

    public ComputerController(ComputerService computerService) {
        this.computerService = computerService;
    }

    @PostMapping("create")
    public ResponseEntity<?> createComputer(@RequestBody CompData compData){
        return  computerService.createComputer(compData);
    }
    @GetMapping("/all")
    public ResponseEntity<?> getAllInstitutes(){
        return computerService.getAllComputers();
    }

    @PatchMapping("/assign/{computerID}/{adminID}")
    public ResponseEntity<?> addAdminToComputer(@PathVariable long computerID, @PathVariable long adminID){
        return computerService.assignAdmin(computerID,adminID);
    }
    @DeleteMapping("/{id}")
    private ResponseEntity<?> deleteComputer(@PathVariable long id) {
        return computerService.deleteComputer(id);
    }

    @PatchMapping("/update/{id}")
    private ResponseEntity<?> updateComputer(@PathVariable long id, @RequestBody CompData compData){ return  computerService.modifyComputer(id,compData);}

    @GetMapping("/log")
    private ResponseEntity<?> getComputersOfLoggedUser(){
        return computerService.getComputersOfLoggedUser();
    }

}
