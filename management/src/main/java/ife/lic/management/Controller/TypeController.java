package ife.lic.management.Controller;
import ife.lic.management.Services.TypeService;

import ife.lic.management.View.TypeData;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin("http://localhost:4200")
@RequestMapping("/type")
@RestController
public class TypeController {
    private final TypeService typeService;


    public TypeController(TypeService typeService) {
        this.typeService = typeService;
    }


    @PostMapping("/create")
    public ResponseEntity<?> createType(@RequestBody TypeData typeData){
        return  typeService.createType(typeData);
    }
    @GetMapping("/all")
    public ResponseEntity<?> getAllTypes(){
        return typeService.getAllTypes();
    }
    @PatchMapping("/update/{id}/{name}")
    public ResponseEntity<?> updateName(@PathVariable long id, @PathVariable String name) {return typeService.updateTypeName(name,id);}


}
