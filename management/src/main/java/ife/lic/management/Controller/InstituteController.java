package ife.lic.management.Controller;

import ife.lic.management.Services.ComputerService;
import ife.lic.management.Services.InstituteService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin("http://localhost:4200")
@RequestMapping("/institute")
@RestController
public class InstituteController {
    private final InstituteService instituteService;

    public InstituteController(InstituteService instituteService) {
        this.instituteService = instituteService;
    }

    @PostMapping("create/{name}/{id}")
    public ResponseEntity<?> createInstitute(@PathVariable String name, @PathVariable Long id){
        return  instituteService.createInstitute(name,id);
    }
    @GetMapping("/all")
    public ResponseEntity<?> getAllInstitutes(){
        return instituteService.getAllInstitutes();
    }

}
