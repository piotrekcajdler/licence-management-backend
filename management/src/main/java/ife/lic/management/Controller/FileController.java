package ife.lic.management.Controller;

import ife.lic.management.Repository.FileRepository;
import ife.lic.management.Services.FileService;
import ife.lic.management.Services.LicenseService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

@Controller
@CrossOrigin("http://localhost:4200")
@RequestMapping("/file")
@RestController
public class FileController {
    private final FileService fileService;
    private final LicenseService licenceService;

    public final static MediaType APPLICATION_OCTET_STREAM_TYPE = new MediaType("application", "octet-stream");
    public final static String APPLICATION_OCTET_STREAM = "application/octet-stream";
    public FileController(FileService fileService, FileRepository fileRepository, LicenseService licenceService) {
        this.fileService = fileService;
        this.licenceService = licenceService;
    }

    @GetMapping("all")
    public ResponseEntity<?> getAllFiles(){ return  new ResponseEntity<>(fileService.getAllFiles(), HttpStatus.OK);}

    @GetMapping("{lic}")
    public ResponseEntity<?> getFilesFromTask(@PathVariable long lic){
        return new ResponseEntity<>(fileService.getFilesForTask(lic),HttpStatus.OK);
    }

    @GetMapping(value = "/getFiles/{fileID}" ,produces = APPLICATION_OCTET_STREAM )
    public void getTaskFiles(@PathVariable long fileID,  HttpServletResponse response) throws IOException {
        fileService.getFile(fileID,response);
    }

    @DeleteMapping("{file}")
    public void deleteFile(@PathVariable  long file){
        fileService.deleteFile(file);
    }
    @PostMapping("/addFile/{id}")
    public void addFile(@RequestParam("file") MultipartFile file, @PathVariable long id) throws IOException { licenceService.saveFile(file,id);}
    @GetMapping("/getFiles/{id}/path")
    public File[] getFilePaths(@PathVariable long id) throws IOException {return licenceService.files(id);}

}
