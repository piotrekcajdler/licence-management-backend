package ife.lic.management.Controller;

import ife.lic.management.Entity.Rolename;
import ife.lic.management.View.PasswordData;
import ife.lic.management.View.PersonData;
import ife.lic.management.Services.PersonService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin("http://localhost:4200")
@RequestMapping("/user")
@RestController
public class UserController {
    private final PersonService personService;

    public UserController(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping("/create")
    public ResponseEntity<?> createUser(@RequestBody PersonData personData){
        return  personService.createUser(personData);
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAllUsers(){
        return personService.getAllPeople();
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable long id){
        return personService.deleteUser(id);
    }
    @GetMapping("/getLogged")
    public ResponseEntity<?> getLogged(){return personService.showLogged();}

    @GetMapping("/getNotAdmins")
    public ResponseEntity<?> getNotAdmins(){
        return personService.getNotAdmins();
    }

    @GetMapping("/getAdmins")
    public ResponseEntity<?> getAdmins(){
        return personService.getAdmins();
    }

    @PatchMapping("/currentUser/change")
    public ResponseEntity<?> changePasswordForCurrentUser(@RequestBody PasswordData passwordData){
        return personService.changeCurrentLoggedUserPassword(passwordData);
    }
    @PatchMapping("/change")
    public ResponseEntity<?> changePasswordForUser(@RequestBody PasswordData passwordData){
        return personService.changeUserPassword(passwordData);
    }
    @PatchMapping("/switch/{id}")
    public ResponseEntity<?> changeUserRoles(@PathVariable long id){
        return personService.switchRoles(id);
    }

}
