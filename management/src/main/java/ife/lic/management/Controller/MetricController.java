package ife.lic.management.Controller;

import ife.lic.management.Services.MetricService;
import ife.lic.management.View.MetricData;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin("http://localhost:4200")
@RequestMapping("/metric")
@RestController
public class MetricController {
    private final MetricService metricService;


    public MetricController(MetricService metricService) {
        this.metricService = metricService;
    }


    @PostMapping("/create")
    public ResponseEntity<?> cretateMetric(@RequestBody MetricData lic){
        return  metricService.createMetric(lic);
    }
    @GetMapping("/all")
    public ResponseEntity<?> getAllMetrics(){
        return metricService.getAllMetrics();
    }

    @GetMapping("/comp/{id}")
    public ResponseEntity<?> getAllMetricForComp(@PathVariable long id){
        return metricService.getAllMetricsForComp(id);
    }

    @GetMapping("/lic/{id}")
    public ResponseEntity<?> getAllMetricForLicence(@PathVariable long id){
        return metricService.getAllMetricsForLic(id);
    }

    @DeleteMapping("/{id}")
    private ResponseEntity<?> deleteMetricRecord(@PathVariable long id) {
        return metricService.deleteMetrics(id);
    }

    @PatchMapping("/update/{id}")
    private ResponseEntity<?> updateMetric(@PathVariable long id,@RequestBody MetricData metricdata) {
        return metricService.modifyMetric(id,metricdata);
    }

    @PatchMapping("/accept/all/{id}")
    private ResponseEntity<?> accept(@PathVariable long id) {
        return metricService.agreeAllMetricsFromComputer(id);
    }
    @PatchMapping("/accept/{id}")
    private ResponseEntity<?> acceptOne(@PathVariable long id) {
        return metricService.agreeOneMetric(id);
    }
}
