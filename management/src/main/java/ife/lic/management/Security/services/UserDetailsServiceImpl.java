package ife.lic.management.Security.services;

import ife.lic.management.Entity.Person;
import ife.lic.management.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class UserDetailsServiceImpl  implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Person person = userRepository.findUserByEmail(username).orElseThrow(
                () -> new UsernameNotFoundException("Person Not Found with -> username: " + username));
        return UserPrinciple.build(person);
    }

}
