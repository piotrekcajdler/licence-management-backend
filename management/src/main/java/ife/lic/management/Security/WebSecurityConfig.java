package ife.lic.management.Security;

import ife.lic.management.Security.jwt.JwtAuthEntryPoint;
import ife.lic.management.Security.jwt.JwtAuthTokenFilter;
import ife.lic.management.Security.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    private JwtAuthEntryPoint unauthorizedHandler;

    @Bean
    public JwtAuthTokenFilter authenticationJwtTokenFilter() {
        return new JwtAuthTokenFilter();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable().
                authorizeRequests()

               .antMatchers("/computer/log").hasAnyRole("SUPER_ADMIN","ADMIN")
               .antMatchers("/computer/create").hasRole("SUPER_ADMIN")
                .antMatchers("/computer/all").hasAnyRole("ADMIN","SUPER_ADMIN")
                .antMatchers("/computer/assign/{computerID}/{adminID}").hasRole("SUPER_ADMIN")
                .antMatchers("/computer/{id}").hasRole("SUPER_ADMIN")
                .antMatchers("/computer/update/{id}").hasRole("SUPER_ADMIN")

                .antMatchers("/institute/all").hasAnyRole("ADMIN","SUPER_ADMIN")
                .antMatchers("/institute/create/{name}/{id}").hasRole("SUPER_ADMIN")

                .antMatchers("/license/all").hasAnyRole("ADMIN","SUPER_ADMIN")
                .antMatchers("/license/create").hasRole("SUPER_ADMIN")
                .antMatchers("/license/{id}").hasRole("SUPER_ADMIN")
                .antMatchers("/license/update/{id}").hasRole("SUPER_ADMIN")

                .antMatchers("/metric/create").hasAnyRole("ADMIN","SUPER_ADMIN")
                .antMatchers("/metric/all").hasAnyRole("ADMIN","SUPER_ADMIN")
                .antMatchers("/metric/comp/{id}").hasAnyRole("ADMIN","SUPER_ADMIN")
                .antMatchers("/metric/lic/{id}").hasAnyRole("ADMIN","SUPER_ADMIN")
                .antMatchers("/metric/{id}").hasAnyRole("ADMIN","SUPER_ADMIN")
                .antMatchers("/metric/update/{id}").hasAnyRole("ADMIN","SUPER_ADMIN")
                 .antMatchers("/accept/all/{id}").hasRole("SUPER_ADMIN")
                .antMatchers("/accept/{id}").hasRole("SUPER_ADMIN")


                .antMatchers("/restApi/auth/signin").permitAll()
                .antMatchers("/restApi/auth/signup").permitAll()
                .antMatchers("/restApi/auth/grant/{email}").hasRole("SUPER_ADMIN")

                .antMatchers("/type/create/{name}").hasRole("SUPER_ADMIN")
                .antMatchers("/type/all").hasAnyRole("ADMIN","SUPER_ADMIN")
                .antMatchers("/type/update/{id}/{name}").hasRole("SUPER_ADMIN")

                .antMatchers("/user/getLogged").hasAnyRole("ADMIN","SUPER_ADMIN")
                .antMatchers("/user/getNotAdmins").hasAnyRole("ADMIN","SUPER_ADMIN")
                .antMatchers("/user/getAdmins").hasAnyRole("ADMIN","SUPER_ADMIN")
                .antMatchers("/user/create").hasRole("SUPER_ADMIN")
                .antMatchers("/user/all").hasAnyRole("ADMIN","SUPER_ADMIN")
                .antMatchers("/user/{id}").hasRole("SUPER_ADMIN")
                .antMatchers("/user/currentUser/change").hasAnyRole("ADMIN","SUPER_ADMIN")
                 .antMatchers("/user/change").hasRole("SUPER_ADMIN")
                .antMatchers("/user/switch/{id}").hasRole("SUPER_ADMIN")

				.anyRequest().authenticated()
                .and()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);

    }
}
