package ife.lic.management.View;

import com.sun.istack.NotNull;
import ife.lic.management.Entity.Rolename;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
public class PersonData {
    @NotNull
    private Rolename role;

    @NotNull
    @Size(min = 5)
    @Email
    private String email;

    @NotNull
    @Size(min = 8)
    private String password;

    @NotNull
    private String name;

    @NotNull
    private String surname;


    private long institute;


}
