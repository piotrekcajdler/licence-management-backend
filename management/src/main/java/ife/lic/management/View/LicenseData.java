package ife.lic.management.View;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import ife.lic.management.Entity.File;
import ife.lic.management.Entity.Metric;
import ife.lic.management.Entity.Type;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
public class LicenseData {

    private Long licenceID;

    private String name;
    @ManyToOne
    @NotNull
    private Long type;

    private String PurchaseDate;
    @Nullable
    private String ExpiryDate;
    @Nullable
    private boolean isActive;
    @Nullable
    private Long AvailableInstallations;
    @Nullable
    private String key;
    @Nullable
    private String description;
    @Nullable
    private Set<File> files;
}
