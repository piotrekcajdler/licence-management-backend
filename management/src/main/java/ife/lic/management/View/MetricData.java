package ife.lic.management.View;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import ife.lic.management.Entity.Computer;
import ife.lic.management.Entity.License;
import ife.lic.management.Entity.Metric;
import ife.lic.management.Entity.Type;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class MetricData {

    @NotNull
    private Long licenseId;
    @NotNull
    private Long computerId;
    @NotNull
    private String InstallDate;
    @Nullable
    private String UninstallDate;
    @NotNull
    private String WhoInstalled;
    @Nullable
    private String WhoUninstalled;
    @NotNull
    private String whoAgreed;
    @Nullable
    private String description;

}
