package ife.lic.management.View;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor

public class PasswordData {
    long ID;
    String password;

}
