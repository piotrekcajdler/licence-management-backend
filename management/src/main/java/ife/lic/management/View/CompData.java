package ife.lic.management.View;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import ife.lic.management.Entity.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class CompData {
    @Nullable
    private String name;
    @NotNull
    private String Location;
    @NotNull
    private boolean IsMultiuser;
    @Nullable
    private String description;
    @Nullable
    private List<String> usersOfComputer;
}
