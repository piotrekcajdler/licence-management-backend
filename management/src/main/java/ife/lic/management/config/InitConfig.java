package ife.lic.management.config;

import java.sql.SQLOutput;
import java.util.HashSet;
import java.util.Set;

import ife.lic.management.Entity.*;
import ife.lic.management.Repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import org.apache.commons.io.FileUtils;


@Component
@AllArgsConstructor
public class InitConfig implements ApplicationRunner {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final LicenceRepository licenceRepository;
    private final TypeRepository typeRepository;
    private final ComputerRepository computerRepository;
    private final InstituteRepository instituteRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Role adminRole = new Role(Rolename.ROLE_ADMIN);
        Role superAdminRole = new Role(Rolename.ROLE_SUPER_ADMIN);
        
        Type box = new Type("Box","DUMMY TEXT");
        Type oem = new Type("Oem","DUMMY TEXT");
        Type shareware = new Type("Shareware","DUMMY TEXT");
        Type adware = new Type("Adware","DUMMY TEXT");
        Type trial = new Type("Trial","DUMMY TEXT");
        Type freeware = new Type("Freeware","DUMMY TEXT");
        Type gpl = new Type("Gpl","DUMMY TEXT");
        Type unlimited = new Type("Unlimited","DUMMY TEXT");
        Institute i = new Institute("I21", 0L);
        FileUtils.deleteDirectory(new java.io.File("src/main/resources/springFiles"));
        FileUtils.forceMkdir(new java.io.File("src/main/resources/springFiles"));
        if(instituteRepository.findAll().isEmpty()){

            instituteRepository.save(i);
            System.out.println(i.getId());
        }
        if(roleRepository.findAll().isEmpty()){
            roleRepository.save(adminRole);
            roleRepository.save(superAdminRole);
        }
        if(userRepository.findAll().isEmpty()){
            Person user = new Person("admin1@admin.pl", passwordEncoder.encode("admin"), "Konrad", "Admin", i);
            Set<Role> roles = new HashSet<>();
            roles.add(superAdminRole);
            user.setRoles(roles);
            userRepository.save(user);

            Person user3 = new Person("admin2@admin.pl", passwordEncoder.encode("admin"), "Maciej", "Admin", i);
            user3.setRoles(roles);
            userRepository.save(user3);

            Person user1 = new Person("user1@user.pl", passwordEncoder.encode("user"), "Jan", "User", i);
            Set<Role> roles1 = new HashSet<>();
            roles1.add(adminRole);
            user1.setRoles(roles1);
            userRepository.save(user1);

            Person user2 = new Person("user2@user.pl", passwordEncoder.encode("user"), "Adam", "User", i);
            user2.setRoles(roles1);
            userRepository.save(user2);
        }
        
        if(typeRepository.findAll().isEmpty()){
            typeRepository.save(box);
            typeRepository.save(oem);
            typeRepository.save(shareware);
            typeRepository.save(adware);
            typeRepository.save(trial);
            typeRepository.save(freeware);
            typeRepository.save(gpl);
            typeRepository.save(unlimited);
        }
        
        if(licenceRepository.findAll().isEmpty()){
            License office = new License("Microsoft 365 Personal", box, "03.09.2021", "03.09.2023", (long) 1, "]52'-Z/Vk-a5wS-bQY$");
            License windows = new License("Microsoft Windows 10", unlimited, "12.06.2021", "12.12.2022", (long) -1, "]52'-Z/Vk-a5wS-bQY$");
            License norton = new License("Symantec Norton Internet Security Deluxe", box, "02.04.2023", "02.04.2022", (long) 50, "]52'-Z/Vk-a5wS-bQY$");
            License eset = new License("ESET NOD32 Antywirus", box, "04.03.2020", "04.03.2025", (long) 100, "]52'-Z/Vk-a5wS-bQY$");
            License autocad = new License("AUTOCAD", box, "03.09.2021", "03.03.2022", (long) 2, "]52'-Z/Vk-a5wS-bQY$");
            License adobe = new License("Adobe Acrobat Reader", box, "03.09.2021", "03.09.2023", (long) -1, "]52'-Z/Vk-a5wS-bQY$");
            License testowa_zero = new License("Testowa, zero available", box, "03.09.2021", "03.09.2023", (long) 0, "]52'-Z/Vk-a5wS-bQY$");
            License testowa_data = new License("Testowa, zła data", box, "03.09.2020", "03.09.2021", (long) 100, "]52'-Z/Vk-a5wS-bQY$");

            licenceRepository.save(office);
            licenceRepository.save(windows);
            licenceRepository.save(norton);
            licenceRepository.save(eset);
            licenceRepository.save(autocad);
            licenceRepository.save(adobe);
            licenceRepository.save(testowa_zero);
            licenceRepository.save(testowa_data);
        }
        
        if(computerRepository.findAll().isEmpty()){
            Computer comp1 = new Computer("komputer 1", "sala 301", true, true);
            Computer comp2 = new Computer("komputer 2", "sala 301", true, true);
            Computer comp3 = new Computer("komputer 3", "sala 301", true, true);
            Computer laptop1 = new Computer("laptop 1", "sala 301", false, true);

            Computer comp4 = new Computer("komputer 1", "sala 302", true, true);
            Computer comp5 = new Computer("komputer 1", "sala 302", true, true);
            Computer comp6 = new Computer("komputer 1", "sala 302", true, true);

            Computer comp7 = new Computer("komputer 1", "sala 314", true, true);
            Computer laptop2 = new Computer("komputer 1", "sala 314", false, true);
            Computer laptop3 = new Computer("komputer 1", "sala 314", false, true);

            computerRepository.save(comp1);
            computerRepository.save(comp2);
            computerRepository.save(comp3);
            computerRepository.save(laptop1);

            computerRepository.save(comp4);
            computerRepository.save(comp5);
            computerRepository.save(comp6);

            computerRepository.save(comp7);
            computerRepository.save(laptop2);
            computerRepository.save(laptop3);
        }

    }
}
