package ife.lic.management.Services;

import ife.lic.management.Entity.Person;
import ife.lic.management.Entity.Role;
import ife.lic.management.Entity.Rolename;
import ife.lic.management.Repository.InstituteRepository;
import ife.lic.management.Repository.RoleRepository;
import ife.lic.management.Repository.UserRepository;
import ife.lic.management.View.PasswordData;
import ife.lic.management.View.PersonData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class PersonService {
    @Autowired
    private PasswordEncoder passwordEncoder;
    private UserRepository userRepository;
    private final InstituteRepository instituteRepository;
    private final RoleRepository roleRepository;

    public PersonService(InstituteRepository instituteRepository, RoleRepository roleRepository,UserRepository userRepository) {
        this.instituteRepository = instituteRepository;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    public ResponseEntity<?> createUser(PersonData personData){
        Person p = new Person(personData.getSurname(),
                personData.getPassword(), personData.getName(), personData.getEmail());
        if (instituteRepository.findById(personData.getInstitute()).isPresent())
            p.setInstitute(instituteRepository.findById(personData.getInstitute()).get());
        userRepository.save(p);
        return new ResponseEntity<>(p,HttpStatus.CREATED);

    }

    public ResponseEntity<?> deleteUser(long id){
        if (userRepository.findById(id).isPresent()){
            Person p = userRepository.findById(id).get();
           p.setActive(false);
            userRepository.save(p);

            return new ResponseEntity<>(HttpStatus.OK);}
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<?> grantAuthorites(String email){
        if (userRepository.findUserByEmail(email).isPresent()){
            Person user = userRepository.findUserByEmail(email).get();

            user.getRoles().add(roleRepository.findByName(Rolename.ROLE_SUPER_ADMIN).get());
            userRepository.save(user);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    public ResponseEntity<?> showLogged(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = ((UserDetails)principal).getUsername();
        Person user = userRepository.findUserByEmail(username).get();
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    public ResponseEntity<?> getAllPeople(){
        return new ResponseEntity<>(userRepository.findAllByActiveIsTrue(),HttpStatus.OK);
    }

    public ResponseEntity<?> getNotAdmins(){
        return new ResponseEntity<>(userRepository.findAllByRolesIsNotContainingAndActiveIsTrue(roleRepository.findByName(Rolename.ROLE_SUPER_ADMIN).get()),HttpStatus.OK);
    }
    public ResponseEntity<?> getAdmins(){
        return new ResponseEntity<>(userRepository.findAllByRolesContainingAndActiveIsTrue(roleRepository.findByName(Rolename.ROLE_SUPER_ADMIN).get()),HttpStatus.OK);
    }

    public ResponseEntity<?> changeCurrentLoggedUserPassword(PasswordData passwordData){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = ((UserDetails)principal).getUsername();
        Person user = userRepository.findUserByEmail(username).get();
        user.setPassword(passwordEncoder.encode(passwordData.getPassword()));
        userRepository.save(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    public ResponseEntity<?> changeUserPassword(PasswordData passwordData){
        Person user = userRepository.findById(passwordData.getID()).get();
        user.setPassword(passwordEncoder.encode(passwordData.getPassword()));
        userRepository.save(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    public ResponseEntity<?> switchRoles(long userid){
        Person user = userRepository.findById(userid).get();
        if (user.getRoles().contains(roleRepository.findByName(Rolename.ROLE_ADMIN).get()) && user.getRoles().size() == 1 ){
            user.getRoles().add(roleRepository.findByName(Rolename.ROLE_SUPER_ADMIN).get());
            userRepository.save(user);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        }
        else {
            Set<Role> roles = new HashSet<>();
            roles.add(roleRepository.findByName(Rolename.ROLE_ADMIN).get());
            user.setRoles(roles);
            userRepository.save(user);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        }
    }


}
