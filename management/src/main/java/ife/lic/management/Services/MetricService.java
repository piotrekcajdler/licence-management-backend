package ife.lic.management.Services;


import ife.lic.management.Entity.*;
import ife.lic.management.Repository.*;
import ife.lic.management.View.LicenseData;
import ife.lic.management.View.MetricData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MetricService {
    @Autowired
    private MetricRepository metricRepository;
    @Autowired
    private ComputerRepository computerRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private LicenceRepository licenceRepository;


    public ResponseEntity<?> createMetric(MetricData metricData){
        Metric m = new Metric(metricData.getInstallDate(),metricData.getUninstallDate(),
                metricData.getWhoInstalled(),metricData.getWhoUninstalled(),false, metricData.getWhoAgreed(),metricData.getDescription());

        if (computerRepository.findById(metricData.getComputerId()).isPresent() && licenceRepository.findById(metricData.getLicenseId()).isPresent()){
           Computer computer = computerRepository.findById(metricData.getComputerId()).get();
            m.setComputer(computer);
            License license = licenceRepository.findById(metricData.getLicenseId()).get();
            if (license.getAvailableInstallations() <= 0 ){
                return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            if(m.getUninstallDate() != null){
                license.setAvailableInstallations(license.getAvailableInstallations()+1);
            }
            else
                license.setAvailableInstallations(license.getAvailableInstallations()-1);
            licenceRepository.save(license);
            m.setLicense(license);
            metricRepository.save(m);
            computer.getMetricList().add(m);
            computerRepository.save(computer);
            return new ResponseEntity<>(m,HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    public ResponseEntity<?> getAllMetrics(){
        return new ResponseEntity<>(metricRepository.findAll(),HttpStatus.OK);
    }
    public ResponseEntity<?> getAllMetricsForComp(long id){
        return new ResponseEntity<>(metricRepository.findAllByComputerId(id),HttpStatus.OK);
    }
    public ResponseEntity<?> getAllMetricsForLic(long id){
        return new ResponseEntity<>(metricRepository.findAllByLicenseId(id),HttpStatus.OK);
    }


    public ResponseEntity<?> deleteMetrics(long id){
        if (metricRepository.findById(id).isPresent()){

            Metric lic = metricRepository.findById(id).get();
            License license = licenceRepository.findById(lic.getLicense().getId()).get();
            license.setAvailableInstallations(license.getAvailableInstallations()+1);
            licenceRepository.save(license);
            lic.setDeleted(true);
            metricRepository.save(lic);
            return new ResponseEntity<>(lic,HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<?> modifyMetric(long id, MetricData metricData){
        if (metricRepository.findById(id).isPresent()){
            Metric metric = metricRepository.findById(id).get();

            Long licenceID = metricData.getLicenseId();
            Long computerID = metricData.getComputerId();
            String installDate = metricData.getInstallDate();
            String uninstallDate = metricData.getUninstallDate();
            String whoInstalled = metricData.getWhoInstalled();
            String whoUninstalled = metricData.getWhoUninstalled();
            String whoAgreed = metricData.getWhoAgreed();
            String description = metricData.getDescription();

            if (licenceID != null && licenceRepository.findById(licenceID).isPresent() ){
                License old = licenceRepository.findById(metric.getLicense().getId()).get();
                old.setAvailableInstallations(old.getAvailableInstallations()+1);
                licenceRepository.save(old);

                License newLic = licenceRepository.findById(licenceID).get();
                if (newLic.getAvailableInstallations() <= 0 ) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                newLic.setAvailableInstallations(newLic.getAvailableInstallations()-1);
                licenceRepository.save(newLic);

                metric.setLicense(licenceRepository.findById(licenceID).get());
            }

            if (computerID != null && computerRepository.findById(computerID).isPresent() ){
              metric.setComputer(computerRepository.findById(computerID).get());
            }

            if (installDate != null && !installDate.equals(metric.getInstallDate())){
                metric.setInstallDate(installDate);
            }
            if (uninstallDate != null && !uninstallDate.equals(metric.getUninstallDate())){
                metric.setUninstallDate(uninstallDate);
            }
            if (whoInstalled != null && !whoInstalled.equals(metric.getWhoInstalled())){
                metric.setWhoInstalled(whoInstalled);
            }
            if (whoUninstalled != null && !whoUninstalled.equals(metric.getWhoUninstalled())){
                metric.setWhoUninstalled(whoUninstalled);
            }
            if (whoAgreed != null && !whoAgreed.equals(metric.getWhoAgreed())){
                metric.setWhoAgreed(whoAgreed);
            }
            if (description != null && !description.equals(metric.getDescription())){
                metric.setDescription(description);
            }
            metricRepository.save(metric);
            return new ResponseEntity<>(metric,HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<?> agreeAllMetricsFromComputer(long id){
        List<Metric> metricList = metricRepository.findAllByComputerIdAndWhoAgreed(id,"-").get();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = ((UserDetails)principal).getUsername();
        Person user = userRepository.findUserByEmail(username).get();
        for (Metric m : metricList){
            m.setWhoAgreed(user.getName() + " " + user.getSurname());
        }
        metricRepository.saveAll(metricList);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    public ResponseEntity<?> agreeOneMetric(long id){
      Metric m = metricRepository.findByIdAndWhoAgreed(id,"-").get();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = ((UserDetails)principal).getUsername();
        Person user = userRepository.findUserByEmail(username).get();
        m.setWhoAgreed(user.getName() + " " + user.getSurname());
        metricRepository.save(m);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }



}
