package ife.lic.management.Services;


import ife.lic.management.Entity.Institute;
import ife.lic.management.Entity.Type;
import ife.lic.management.Repository.InstituteRepository;
import ife.lic.management.Repository.TypeRepository;
import ife.lic.management.View.TypeData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class TypeService {
    @Autowired
    private TypeRepository typeRepository;

    public ResponseEntity<?> createType(TypeData typeData){
        if (typeRepository.findByName(typeData.getName()).isEmpty()) {
            Type type = new Type(typeData.getName(),typeData.description);
            typeRepository.save(type);
            return new ResponseEntity<>(type, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }
    public ResponseEntity<?> getAllTypes(){
        return new ResponseEntity<>(typeRepository.findAll(),HttpStatus.OK);
    }

    public ResponseEntity<?> updateTypeName(String name,long id){
        if (typeRepository.findById(id).isPresent()){
            Type type = typeRepository.findById(id).get();
            type.setName(name);
            typeRepository.save(type);
            return  new ResponseEntity<>(type,HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


}
