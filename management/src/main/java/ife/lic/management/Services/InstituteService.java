package ife.lic.management.Services;

import ife.lic.management.Entity.Institute;
import ife.lic.management.Repository.ComputerRepository;
import ife.lic.management.Repository.InstituteRepository;
import ife.lic.management.Repository.LicenceRepository;
import ife.lic.management.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class InstituteService {
    @Autowired
    private InstituteRepository instituteRepository;
    @Autowired
    private ComputerRepository computerRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private LicenceRepository licenceRepository;

    public ResponseEntity<?> createInstitute(String name, Long parentID){
        if (instituteRepository.findByName(name).isEmpty()) {
            Institute institute = new Institute(name, parentID);
            instituteRepository.save(institute);
            return new ResponseEntity<>(institute, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }
    public ResponseEntity<?> getAllInstitutes(){
       return new ResponseEntity<>(instituteRepository.findAll(),HttpStatus.OK);
    }

}
