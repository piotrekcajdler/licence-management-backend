package ife.lic.management.Services;

import ife.lic.management.Entity.*;
import ife.lic.management.Repository.ComputerRepository;
import ife.lic.management.Repository.UserRepository;
import ife.lic.management.View.CompData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ComputerService {
    @Autowired
    private ComputerRepository computerRepository;
    @Autowired
    private UserRepository userRepository;

    public ResponseEntity<?> createComputer(CompData compData){

            Computer computer = new Computer(compData.getName(), compData.getLocation(), compData.isIsMultiuser(),true,compData.getDescription());
            computerRepository.save(computer);
            return new ResponseEntity<>(computer, HttpStatus.OK);

    }
    public ResponseEntity<?> getAllComputers(){
        return new ResponseEntity<>(computerRepository.findAll(),HttpStatus.OK);
    }

    public ResponseEntity<?> assignAdmin(long computerid, long adminid){
        if (userRepository.findById(adminid).isPresent()){
        Person admin = userRepository.findById(adminid).get();
        if (computerRepository.findById(computerid).isPresent()){
            Computer computer = computerRepository.findById(computerid).get();
            if(admin.getComputerList().contains(computer))
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            admin.getComputerList().add(computer);
            userRepository.save(admin);
            return new ResponseEntity<>(computer,HttpStatus.ACCEPTED);
        }
        else return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    public ResponseEntity<?> deleteComputer(long id){
        if(computerRepository.findById(id).isPresent()){
        Computer comp = computerRepository.findById(id).get();
        comp.setStillUsed(false);
        computerRepository.save(comp);
        return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    public ResponseEntity<?> modifyComputer(long id, CompData compData){
        if (computerRepository.findById(id).isPresent()){
            Computer computer = computerRepository.findById(id).get();
            String name = compData.getName();
            boolean isMultiuser = compData.isIsMultiuser();
            String location = compData.getLocation();
            String description = compData.getDescription();
            List<String> users = compData.getUsersOfComputer();


            if (users!= null ){
              for( String user : users){
                  computer.getUsersOfComputer().add(user);
              }
            }

            if (  name != null && !name.equals(computer.getName())){
                computer.setName(name);
            }
            if ( isMultiuser != computer.isIsMultiuser()){
                computer.setIsMultiuser(isMultiuser);
            }
            if ( location != null && !location.equals(computer.getLocation())){
                computer.setLocation(location);
            }
            if ( description != null  && !description.equals(computer.getDescription())){
                computer.setDescription(description);
            }


            computerRepository.save(computer);
            return new ResponseEntity<>(computer,HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

   public ResponseEntity<?> getComputersOfLoggedUser(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String username = ((UserDetails)principal).getUsername();
        Person user = userRepository.findUserByEmail(username).get();
        return new ResponseEntity<>(computerRepository.findAllByWorkersContaining(user), HttpStatus.OK);
    }

}
