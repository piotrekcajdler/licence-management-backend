package ife.lic.management.Services;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;

import ife.lic.management.Entity.License;
import ife.lic.management.Entity.Type;
import ife.lic.management.Repository.LicenceRepository;
import ife.lic.management.Repository.TypeRepository;
import ife.lic.management.View.LicenseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

@Service
public class LicenseService {
    @Autowired
    private LicenceRepository licenseRepository;
    @Autowired
    private TypeRepository typeRepository;
    public ResponseEntity<?> createLicense(LicenseData licenseData){
       if (licenseRepository.findLicenseByName(licenseData.getName()).isPresent())
           return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        License l = new License(licenseData.getName(),
                licenseData.getPurchaseDate(), licenseData.getExpiryDate(), licenseData.getAvailableInstallations(),
                licenseData.getKey(),licenseData.getDescription());
        if (typeRepository.findById(licenseData.getType()).isPresent()){
            l.setType(typeRepository.findById(licenseData.getType()).get());
        }
        else return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        licenseRepository.save(l);
        return new ResponseEntity<>(l,HttpStatus.CREATED);
    }

    public ResponseEntity<?> getAllLicences(){
        return new ResponseEntity<>(licenseRepository.findAll(),HttpStatus.OK);
    }

    public ResponseEntity<?> deleteLicence(long id){
        if (licenseRepository.findById(id).isPresent()){
            License lic = licenseRepository.findById(id).get();
            lic.setActive(false);
            licenseRepository.save(lic);
            return new ResponseEntity<>(lic,HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<?> modifyLicence(long id,LicenseData licenseData){
        if (licenseRepository.findById(id).isPresent()){

            License lic = licenseRepository.findById(id).get();
            String name = licenseData.getName();
            boolean isActive = licenseData.isActive();
            String PurchaseDate = licenseData.getPurchaseDate();
            String ExpiryDate = licenseData.getExpiryDate();
            Long AvailableInstallations = licenseData.getAvailableInstallations();
            String key = licenseData.getKey();
            String description = licenseData.getDescription();

            if (  licenseData.getLicenceID() != null && typeRepository.findById(licenseData.getLicenceID()).isPresent()){
                lic.setType( typeRepository.findById(licenseData.getLicenceID()).get());
            }
            if ( licenseRepository.findLicenseByName(name).isEmpty() && name != null && !name.equals(lic.getName())){
                lic.setName(name);
            }
            if ( isActive != lic.isActive()){
                lic.setActive(isActive);
            }
            if ( PurchaseDate != null && !PurchaseDate.equals(lic.getPurchaseDate())){
                lic.setPurchaseDate(PurchaseDate);
            }
            if ( ExpiryDate != null  && !ExpiryDate.equals(lic.getExpiryDate())){
                lic.setExpiryDate(ExpiryDate);
            }
            if ( AvailableInstallations != null && !AvailableInstallations.equals(lic.getAvailableInstallations())){
                lic.setAvailableInstallations(AvailableInstallations);
            }
            if (key !=null && !key.equals(lic.getKey())){
                lic.setKey(key);
            }
            if (description!= null && !description.equals(lic.getDescription())){
                lic.setDescription(description);
            }

            licenseRepository.save(lic);
            return new ResponseEntity<>(lic,HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<?> findLicByName(String name){
        if (licenseRepository.findLicenseByName(name).isPresent()){
            return new ResponseEntity<>(licenseRepository.findLicenseByName(name),HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    public void saveFile(MultipartFile file, long licId) throws IOException {
        System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        Path filePath = Path.of("src/main/resources/springFiles/" + licId);
        if (!Files.exists(filePath)){
            File f1 = new File("src/main/resources/springFiles/"+licId);
            f1.mkdir();}

        String folder = "src/main/resources/springFiles/"+licId +'/';
        byte[] bytes = file.getBytes();
        Path path = Paths.get(folder + file.getOriginalFilename());
        Files.write(path,bytes);
       ife.lic.management.Entity.File f = new ife.lic.management.Entity.File(file.getOriginalFilename());
        f.setPath( folder + file.getOriginalFilename());
        License l = licenseRepository.findById(licId).get();
        Set<ife.lic.management.Entity.File> files = l.getFiles();
        files.add(f);
        l.setFiles(files);
        licenseRepository.save(l);

    }

    public File[] files(long taskID) throws IOException {
        File directoryPath = new File("src/main/resources/springFiles/" + taskID);
        return directoryPath.listFiles();
    }
}
