package ife.lic.management.Components;
import ife.lic.management.Entity.License;
import ife.lic.management.Repository.LicenceRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
public class Scheduler {
    LicenceRepository licenceRepository;
    public Scheduler(LicenceRepository licenceRepository) {
        this.licenceRepository = licenceRepository;

    }

    @Scheduled(cron = "0 8 * * * *")
    public void checkDataOfEndings() {
        final LocalDate date = LocalDate.now();
        List<License> tasks = licenceRepository.findLicenseByGivenExpirationDate(date.toString());
        for (License task : tasks) {
            task.setActive(false);
        }
        licenceRepository.saveAll(tasks);
    }
}
