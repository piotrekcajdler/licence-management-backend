package ife.lic.management.Repository;

import ife.lic.management.Entity.License;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface LicenceRepository extends JpaRepository<License,Long> {

    @Query("select l from License l where l.ExpiryDate <= :expiryDate")
    List<License> findLicenseByGivenExpirationDate(@Param("expiryDate") String expiryDate);

    Optional<License> findLicenseByName(String name);

}
