package ife.lic.management.Repository;

import ife.lic.management.Entity.Person;
import ife.lic.management.Entity.Role;
import ife.lic.management.Entity.Rolename;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface UserRepository extends JpaRepository<Person,Long> {
    Optional<Person> findUserByEmail(String email);
    boolean existsByEmail(String email);
    List<Person> findAllByRolesIsNotContainingAndActiveIsTrue(Role role);

    List<Person> findAllByActiveIsTrue();
    List<Person> findAllByRolesContainingAndActiveIsTrue(Role role);
}
