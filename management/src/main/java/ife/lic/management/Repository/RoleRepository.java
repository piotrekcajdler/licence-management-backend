package ife.lic.management.Repository;


import ife.lic.management.Entity.Role;
import ife.lic.management.Entity.Rolename;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(Rolename role);


}
