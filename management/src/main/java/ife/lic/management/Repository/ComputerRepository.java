package ife.lic.management.Repository;

import ife.lic.management.Entity.Computer;
import ife.lic.management.Entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComputerRepository extends JpaRepository<Computer,Long> {
    List<Computer> findAllByWorkersContaining(Person person);
}
