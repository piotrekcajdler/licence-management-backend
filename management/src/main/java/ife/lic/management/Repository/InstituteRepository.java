package ife.lic.management.Repository;

import ife.lic.management.Entity.Institute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InstituteRepository extends JpaRepository<Institute,Long> {
    @Override
    Optional<Institute> findById(Long aLong);
    Optional<Institute> findByName(String name);
}
