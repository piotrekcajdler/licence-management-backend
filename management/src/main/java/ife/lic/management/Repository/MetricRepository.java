package ife.lic.management.Repository;

import ife.lic.management.Entity.Metric;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MetricRepository extends JpaRepository<Metric,Long> {
    Optional<List<Metric>> findAllByComputerId(long id);
    Optional<List<Metric>> findAllByLicenseId(long id);
    Optional<List<Metric>> findAllByComputerIdAndWhoAgreed(long id, String whoAgreed);
    Optional<Metric> findByIdAndWhoAgreed(long id, String whoAgreed);
}
